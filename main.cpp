#include <iostream>
#include <cstdio> //popen
#include <string>
#include <sstream>
#include <cstdlib>
#include <direct.h> //_chdir
#include <algorithm>
#include <vector>
#include <fstream>
#include <windows.h>

bool is_batch_file = false;
bool echo_on = true;
bool quit = false;
bool return_status = 0;

std::vector<std::pair<std::string, int>> labels; //label_name, pos

std::ifstream in_file;

HANDLE h_std_out;
CONSOLE_SCREEN_BUFFER_INFO csbi;
COORD home_coords = {0, 0};

char executable_path[2048];
char current_dir[2048];

static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

void dump_string(std::string s)
{
    for(auto i : s)
    {
        std::cout << (int)i << ',';
    }
}

bool file_exists(std::string filename)
{
    WIN32_FIND_DATA file_data;
    HANDLE find_handle = FindFirstFile(filename.c_str(), &file_data);
    if(find_handle != INVALID_HANDLE_VALUE)
    {
        FindClose(find_handle);
        return true;
    }
    return false;
}

std::string get_command_output(const char * cmd)
{
    std::stringstream ss;
    FILE * pipe_in;
    char buf[512];
    
    if(!(pipe_in = popen(cmd, "r")))
    {
        exit(1);
    }
    
    while(fgets(buf, 512, pipe_in) != NULL)
    {
        ss << buf;
    }
    pclose(pipe_in);
    
    return ss.str();
}

std::vector<std::string> tokenize(std::string cmd)
{
    trim(cmd);
    int prev_space = -1;
    bool ignore_spaces = false;
    std::vector<std::string> splitted_string;
    int i;
    for(i = 0; i < cmd.length(); i++)
    {
        switch(cmd[i])
        {
            case ' ':
            {
                if(ignore_spaces) break;
                if(cmd[prev_space + 1] == '"' && cmd[i - 1] == '"')
                {
                    splitted_string.push_back(cmd.substr(prev_space + 2, i - prev_space - 3));
                }
                else
                {
                    splitted_string.push_back(cmd.substr(prev_space + 1, i - prev_space - 1));
                }
                prev_space = i;
                break;
            }
            case '"':
            {
                ignore_spaces = !ignore_spaces;
            }
        }
    }
    if(i != prev_space)
    {
        if(cmd[prev_space + 1] == '"' && cmd[i - 1] == '"')
        {
            splitted_string.push_back(cmd.substr(prev_space + 2, i - prev_space - 3));
        }
        else
        {
            splitted_string.push_back(cmd.substr(prev_space + 1, i - prev_space - 1));
        } 
    }
    
    return splitted_string;
}

void clrscr()
{
    DWORD count;
    DWORD cell_count;
    
    cell_count = csbi.dwSize.X * csbi.dwSize.Y;
    
    if(!FillConsoleOutputCharacter(
        h_std_out,
        (TCHAR) ' ',
        cell_count,
        home_coords,
        &count
        )) return;
    
    if(!FillConsoleOutputAttribute(h_std_out, csbi.wAttributes, cell_count, home_coords, &count)) return;
    
    SetConsoleCursorPosition(h_std_out, home_coords);
}

void set_color(std::string color)
{
    std::transform(color.begin(), color.end(), color.begin(), ::toupper);
    std::string valid_chars("0123456789ABCDEF");
    
    /*for(int i=0; i<2; i++)
    {
        bool is_in_valid_chars = false;
        for(int j=0; j<16; j++)
        {
            if(color[i] == valid_chars[j])
            {
                is_in_valid_chars = true; break;
            }
        }
        if(!is_in_valid_chars) return;
    }*/
    
    if(color.size() != 2) return;
    
    size_t bg_pos = valid_chars.find_first_of(color[0]);
    size_t fg_pos = valid_chars.find_first_of(color[1]);
    if(bg_pos == std::string::npos || fg_pos == std::string::npos) return; //Als een van de twee onzin is
    
    uint8_t color_int = (uint8_t)fg_pos | ((uint8_t)bg_pos << 4);
    SetConsoleTextAttribute(h_std_out, color_int);
}

void process_bat_file(const char* batch_file_name);
void parse_command(std::string cmd)
{
    if(cmd.length() == 2 && cmd[1] == ':') //Drive change
    {
        cmd[0] = tolower(cmd[0]);
        if(_chdrive(cmd[0] - 'a' + 1) == -1) //_chdrive verwacht een waarde van 1 tot 26
        {
            std::cerr << "The system cannot find the drive specified.\n";
            return;
        }
    }
    
    std::vector<std::string> tokenized = tokenize(cmd);
    if(tokenized[0] == "echo")
    {
        if(tokenized.size() == 1)
        {
            //std::cout << "Echo " << echo_on ? "off" : "on" << ".\n"; 
            if(echo_on)
                std::cout << "Echo on.\n";
            else
                std::cout << "Echo off.\n";
        }
        else
        {
            for(unsigned i=1; i<tokenized.size(); i++)
            {
                std::cout << tokenized[i] << ' ';
            }
        }
        return;
    }
    if(tokenized[0] == "cd")
    {
        if(tokenized.size() > 1)
        {
            if(_chdir(tokenized[1].c_str()))
            {
                std::cout << "Error: " << tokenized[1] << " is not a valid directory.\n";
            }
        }
        return;
    }
    if(tokenized[0] == "exit")
    {
        if(tokenized.size() > 1) ///TODO organize if structure so size gets tested first
        {
            return_status = std::stoi(tokenized[1]);
        }
        quit = true;
        return;
    }
    if(tokenized[0] == "cls" || tokenized[0] == "clear")
    {
        clrscr();
        return;
    }
    if(tokenized[0] == "color")
    {
        if(tokenized.size() > 1)
        {
            set_color(tokenized[1]);
        }
        ///TODO current_color
    }
    if(tokenized[0] == "title")
    {
        if(tokenized.size() > 1)
        {
            SetConsoleTitle(tokenized[1].c_str());
        }
        return;
    }
    if(tokenized.size() == 1)
    {
        if(cmd.length() > 4 && tokenized[0].substr(cmd.length() - 4, 4) == ".bat")
        {
            //parse_command(std::string(executable_path) + std::string(" ") + cmd);
            process_bat_file(cmd.c_str());
            return;
        }
    }
    //std::cout << "Testing for batch file.\n";
    if(tokenized[0] == "@echo")
    {
        if(tokenized.size() > 1)
        {
            echo_on = !(tokenized[1] == "off");
        }
        return;
    }
    if(is_batch_file)
    {
        if(tokenized[0] == "goto")
        {
            if(tokenized.size() > 1)
            {
                for(auto i : labels)
                {
                    if(i.first == tokenized[1])
                    {
                        in_file.seekg(i.second);
                    }
                }
            }
            return;
        }
        if(cmd[0] == ':') //Label
        {
            labels.push_back(std::make_pair(cmd.substr(1), in_file.tellg() - 1));
            return;
        }
        //std::cout << "Done testing for batch file.\n";
    }
    
    std::string batch_extensions[] = { ".bat", ".cmd" };
    
    for(std::string ext : batch_extensions)
    {
        if(file_exists(cmd + ext))
        {
            process_bat_file((cmd + ext).c_str());
            return;
        }
    }
    
    //std::cout << "using popen...\n";
    std::string pipe_out = get_command_output(cmd.c_str());
    std::cout << pipe_out;
}

void process_bat_file(const char * batch_file_name)
{
    is_batch_file = true;
    in_file.open(batch_file_name);
    if(!in_file.good())
    {
        std::cerr << "Please enter a valid filename.\n";
    }
    
    std::string cmd;
    
    while(getline(in_file, cmd) && !quit)
    {
        if(echo_on)
        {
            std::cout << current_dir << ">" << cmd << '\n';
        }
        parse_command(cmd);
    }
    //quit = true;
    in_file.close();
}

int main(int argc, char **argv)
{
    h_std_out = GetStdHandle(STD_OUTPUT_HANDLE); //Initialize console
    if(h_std_out == INVALID_HANDLE_VALUE) return 1;
    
    if(!GetConsoleScreenBufferInfo(h_std_out, &csbi)) return 1; //Get console screen buffer
    
    SetConsoleTitle("Official school command prompt.");
    
    OSVERSIONINFO osvi;
    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx(&osvi);
    std::cout << "Microsoft Windows [Version " << osvi.dwMajorVersion << '.' << osvi.dwMinorVersion  << '.' << osvi.dwBuildNumber << "]\n";
    std::cout << "(c) 1337 Microsoft corporation. All rights reserved.\n";
    
    std::cout.sync_with_stdio(false);
    
    if(!GetModuleFileName(NULL, executable_path, 2048))
    {
        std::cout << "Error getting executable path.\n";
    }
    
    if(!_getcwd(current_dir, 2048))
        return 1;
    
    if(argc > 1)
    {
        process_bat_file(argv[1]);
    }
    while(!quit)
    {
        std::cout << current_dir << ">";
        
        std::string cmd;
        std::getline(std::cin, cmd);
        ///TODO load into memory
        
        parse_command(cmd);
        std::cout << '\n';
    }
    
    return return_status;
}
